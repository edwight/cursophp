<?php
namespace App\Controllers;
use App\Models\Empleado;

class HomeController {
    private $empleado;
    public function __construct(){
        $this->empleado = new Empleado;
    }
    public function index() {
        $empleados = $this->empleado->listar();
        //$empleado = $this->empleado->obtener(1);
        //var_dump($empleado);

        require_once _VIEW_PATH_ . 'header.php';
        require_once _VIEW_PATH_ .'home/index.php';
        require_once _VIEW_PATH_ . 'footer.php';
    }

    public function test(){
        require_once _VIEW_PATH_ . 'header.php';
        require_once _VIEW_PATH_ .'home/test.php';
        require_once _VIEW_PATH_ . 'footer.php';
    }

    public function hello() {
        echo 'hello world';
    }
}