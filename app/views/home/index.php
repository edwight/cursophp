<h2 class="page-header">Home page</h2>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
  	<?php foreach ($empleados as $empleado): ?>
	    <tr>
	      <th scope="row">
	      	<?php echo $empleado->id ?>
	      		
	      </th>
	      <td>
	      	<?php echo $empleado->nombre ?> <?php echo $empleado->apellido ?>
	      		
	      </td>
	      <td>
	      	<?php echo $empleado->fecha_nacimiento ?>
	      		
	      </td>
	      <td>
	      	<button type="button" class="btn btn-danger btn-sm">Eliminar</button>
	      </td>
	    </tr>
	<?php endforeach ?>
  </tbody>
</table>

