<?php
namespace App\Models;
use Exception; 
/**
 * Empleado
 */
class Empleado
{
	private $pdo;
	function __construct()
	{
		$this->pdo = \Core\Database::getConnection();	
	}
	public function listar(){
		$resul = [];
		try{
			$smt = $this->pdo->prepare('select * from empleado');
			$smt->execute();
			$resul = $smt->fetchAll();
		}
		catch(Exception $e){

		}
		return $resul;
	}
	public function eliminar($id):bool{
		$result = false;
		try{
			$smt = $this->pdo->prepare('delete * from empleado where = ?');
			$smt->execute([$id]);
			$result = true;
		}
		catch(Exception $e){

		}
		return $result;
	}
	public function obtener(int $id):Empleado{
		$result = new Empleado;
		try{
			$smt = $this->pdo->prepare('select * from empleado where id = ?');
			$smt->execute([$id]);
			$fetch = $smt->fetch();
			$result->id = $fetch->id;
			$result->nombre = $fetch->nombre;
			$result->apellido = $fetch->apellido;
			$result->fecha_nacimiento = $fetch->fecha_nacimiento;
			$result->empleado_id = $fetch->empleado_id;
		}
		catch(Exception $e){

		}
		return $result;
	}
}